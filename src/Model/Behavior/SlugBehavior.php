<?php
/**
 * Created by PhpStorm.
 * User: Rafał Wołoszka
 * Date: 10.01.17
 * Time: 10:07
 */

namespace gandh1pl\Cake\Slugs\Model\Behavior;

use Cake\Core\Exception\Exception;
use Cake\ORM\Behavior;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Text;

class SlugBehavior extends Behavior {

    /**
     * Default config for this object.
     *
     * - `attributesToGenerateFrom` Array of attributes to generate slug from. Here you can use paths as for Hash::extract().
     * - `minimumAttributesToGenerateFrom` int Minimum amount of attributes required to generate slug.
     *  It can be used to abort process of slug's generation, when some of specified attributes in $attributesToGenerateFrom are empty and total count of not empty attributes isn't greater or equal than this value.
     *  Set 0 to ignore.
     * - `generateSlugOnInsert` bool Specify if slug could be generated automatically after saving new record.
     * - `urlTemplate` string|callable Template for creating standard url of entity.
     *  It can be string or callable. If it's callable, it will be called with entity object as argument.
     *  If it is string, it will be passed to Text::insert() as first parameter, so there can be used placeholders for dynamical attributes, like id.
     * - `urlParameters` array Entity's attributes used as parameters for $urlTemplate. Hash::extract() will be used to get values of attributes.
     *  Values will be passed to Text::insert() as second parameter, when getting standard entity's url.
     *  Ignored when $urlTemplate is callable.
     * - `routeRelationBindingKey` string|false Column name of owner's table used as binding key for relation with routes table.
     * @var array
     * @see Hash::extract()
     * @see Text::insert()
     */
    protected $_defaultConfig = [
        'attributesToGenerateFrom' => [],
        'minimumAttributesToGenerateFrom' => 1,
        'generateSlugOnInsert' => true,
        'urlTemplate' => false,
        'urlParameters' => [],
        'routeRelationBindingKey' => 'route_id'
    ];

    /**
     * @inheritdoc
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        if(empty($this->config('attributesToGenerateFrom'))) {
            throw new Exception("To use SlugBehavior with " . get_class($this->_table) . " you must specify config field of attributes used to generate slug from ('attributesToGenerateFrom').");
        }

        if(!is_callable($this->config('urlTemplate')) && empty($this->config('urlTemplate'))) {
            throw new Exception("To use SlugBehavior with " . get_class($this->_table) . " you must specify config field 'urlTemplate'.");
        }

        $this->Routes = TableRegistry::get('Routes');
        if(($routeRelationBindingKey = $this->config('routeRelationBindingKey')) !== false) {
            $this->_table->belongsTo('Routes', [
                'foreignKey' => $routeRelationBindingKey,
                'bindingKey' => 'id',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        if($entity->isNew() && $this->config('generateSlugOnInsert')) {
            $this->generateSlug($entity);
        }
    }

    /**
     * Zwraca slug wygenerowany na podstawie atrybutów encji.
     * @param $entity
     * @return string|bool Wygenerowany slug lub false gdy nie można wygenerować (w przypadku braku odpowiedniej ilości dołączonych atrybutów, na podstawie których funkcja generuje slug)
     */
    public function generateSlugValue($entity) {
        $parts = [];
        foreach($this->config('attributesToGenerateFrom') as $attribute) {
            $values = Hash::extract($entity, $attribute);
            $value = isset($values[0]) ? $values[0] : null;
            if(!empty($value)) {
                $parts[] = Text::slug($value);
            }
        }
        if($this->config('minimumAttributesToGenerateFrom') && $this->config('minimumAttributesToGenerateFrom') > count($parts)) {
            return false;
        }
        return '/' . strtolower(implode('-', $parts));
    }

    /**
     * Zapisuje w bazie danych slug oraz zmienia pole `router_id` encji właściciela na id zapisanego slugu
     * @param $entity Encja produktu / kategorii
     * @param $slugEntity Encja slug'a, najlepiej jak stworzona przez metodę createSlug()
     * @return bool
     */
    public function saveSlug($entity, $slugEntity) {
        if($this->Routes->save($slugEntity, ['checkExisting' => false])) {
            $entity['route'] = $slugEntity;
            if(!$entity['route_id'] || $entity['route_id'] !== $slugEntity->id) {
                $entity['route_id'] = $slugEntity->id;
                $this->_table->save($entity);
            }
            return true;
        }
        return false;
    }

    /**
     * Tworzy i wypełnia danymi model nowego sluga danego produktu / kategorii / czegokolwiek.
     * Jeżeli $redirect nie jest null'em (czyli tworzony slug będzie przekierowaniem), pole url zostanie ustawione na główny slug.
     *
     * @param $entity
     * @param string|null $value
     * @param int|null $redirect
     * @return mixed
     */
    public function createSlug($entity, $value = null, $redirect = null) {
        if($redirect) {
            $mainSlugEntity = $this->getSlug($entity);
        }
        $slugEntity = $this->Routes->newEntity([
            'slug' => $value,
            'url' => ($redirect && $mainSlugEntity ? $mainSlugEntity->slug : $this->getSlugUrl($entity)),
            'redirect' => $redirect
        ]);
        $slugEntity->owner = $entity;
        return $slugEntity;
    }

    /**
     * Zwraca obiekt GŁÓWNEGO slugu danej encji (produktu / kategorii).
     * Jeżeli slug nie istnieje, zostanie stworzona nowa encja produktu.
     * Jako drugi parametr można podać nową wartość slug'u, która zostanie bezpiecznie ustawiona (z walidacją poprawnośći).
     *
     * @param $entity
     * @param $slugValue|null Jeżeli podano wartość slugu, zostanie on ustawiony w bezpieczny sposób (z walidacją, która wymusi "/" na początku slugu)
     * @return Entity
     */
    public function getSlug($entity, $slugValue = null) {
        if(isset($entity['route'])) {
            $model = $entity['route'];
        } else {
            $url = $this->getSlugUrl($entity);
            $model = $this->Routes->getRouteByUrl($url);
            if(!$model) {
                return $this->createSlug($entity, $slugValue);
            }
        }
        if(!($model instanceof EntityInterface)) {
            $model = $this->internalCreateSlugEntity($model);
        }
        if($slugValue) {
            // Metoda poniżej wywoła event Model.beforeMarshal, w której implementacji znajduje się walidacja slug'a.
            $this->Routes->patchEntity($model, ['slug' => $slugValue]);
        }
        $model->setOwner($entity);
        return $model;
    }

    /**
     * Zwraca obiekty wszystkich slugów - główny oraz te "przekierowujące" - z ustawionym polem `redirect`
     * @param $entity
     * @return Route[]
     */
    public function getSlugs($entity) {
        $url = $this->getSlugUrl($entity);
        $models = [];
        foreach($this->Routes->getRoutesByUrl($url) as $i => $slug) {
            if(!($slug instanceof EntityInterface)) {
                $slug = $this->internalCreateSlugEntity($slug);
            }
            $models[] = $slug;
        }
        return $models;
    }

    /**
     * Create entity of existing record from data in safe way (using patchEntity, which fires beforeMarshall event)
     * @param $values
     */
    protected function internalCreateSlugEntity($values) {
        $slugEntity = $this->Routes->newEntity();
        $originalAccessible = $slugEntity->accessible('*');
        // in this way we can assign properties like 'id', that could be not assignable normally
        $slugEntity->accessible('*', true);
        $this->Routes->patchEntity($slugEntity, $values);
        $slugEntity->isNew(false);
        // fixing potential security risk ;)
        $slugEntity->accessible('*', $originalAccessible);
        return $slugEntity;
    }

    /**
     * Return standard entity's url, using config's urlTemplate value.
     * @param EntityInterface $entity
     * @return string
     */
    public function getSlugUrl($entity) {
        $urlTemplate = $this->config('urlTemplate');
        if(is_callable($urlTemplate)) {
            return call_user_func_array($urlTemplate, [$entity]);
        }
        $params = [];
        foreach($this->config('urlParameters') as $key => $param) {
            $value = Hash::extract($entity, $param);
            if(isset($value[0]) && !empty($value[0])) {
                $params[$key] = $value[0];
            }
        }
        return Text::insert($urlTemplate, $params);
    }

    /**
     * Zapisuje w bazie danych slug z wygenerowaną wartością.
     * @param $entity
     * @return bool|EntityInterface|mixed Wynik zapisu slug'u
     */
    public function generateSlug($entity) {
        $slugEntity = $this->getSlug($entity);
        if(($slugValue = $this->generateSlugValue($entity)) !== false) {
            $slugEntity->slug = $slugValue;
            return $this->saveSlug($entity, $slugEntity) ? $slugEntity : false;
        }
        return null;
    }
}