<?php
/**
 * Created by PhpStorm.
 * User: xubuntu
 * Date: 05.01.17
 * Time: 16:01
 */

namespace gandh1pl\Cake\Slugs\Model\Entity;

use Cake\ORM\Entity;


class Route extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_owner = null;

    public function getOwner() {
        return $this->_owner;
    }

    public function setOwner($value) {
        $this->_owner = $value;
    }
}