<?php
/**
 * Created by PhpStorm.
 * User: Rafał Wołoszka
 * Date: 05.01.17
 * Time: 16:04
 */

namespace gandh1pl\Cake\Slugs\Model\Table;

use App\Model\Entity\Route;
use Cake\Cache\Cache;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\ResultSet;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\Query;
use ArrayObject;
use Cake\Validation\Validator;

/**
 * Class RoutesTable
 *
 * @package App\Model\Table
 */
class RoutesTable extends Table
{

    protected $_cacheEngine = 'slugs';

    protected $_finders = ['slug', 'url'/*, 'id'*/];

    /**
     * Name of route's table. Defaults to `routes`, but you can change it.
     * @var string
     */
    protected $tableName = 'routes';

    /**
     * Preferred hydrate option when making query to database.
     * @var bool
     */
    protected $defaultHydrate = false;

    /**
     * @inheritdoc
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table($this->tableName);
        $this->displayField('slug');
        $this->primaryKey('id');
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->integer('redirect')
            ->allowEmpty('redirect', true);

        return $validator;
    }

    /**
     * @inheritdoc
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['slug']));
        return $rules;
    }



    /**
     * @inheritdoc
     */
    public function implementedEvents()
    {
        return [
            'Model.afterSave' => 'afterSave',
            'Model.beforeMarshal' => 'beforeMarshal',
        ];
    }

    /**
     * Zwraca rekord dla danego slug.
     *
     * @param $slug
     * @return Route|null
     */
    public function getRouteBySlug($slug)
    {
        return $this
            ->find()
            ->cache($this->getSlugCacheKey('slug', $slug), 'slugs')
            ->where([$this->alias().'.slug' => $slug])
            ->hydrate($this->defaultHydrate)
            ->first();
    }

    /**
     * Zwraca rekord dla danego standardowego url.
     *
     * @param $url
     * @return Route|null
     */
    public function getRouteByUrl($url)
    {
        return $this
            ->find()
            ->cache($this->getSlugCacheKey('url', $url), 'slugs')
            ->where([
                $this->alias().'.url' => $url,
                $this->alias().'.redirect IS NULL'
            ])
            ->hydrate($this->defaultHydrate)
            ->first();
    }


    /**
     * Zwraca rekordy dla danego standardowego url.
     *
     * @param $url
     * @return Route[]|null
     */
    public function getRoutesByUrl($url)
    {
        return $this
            ->find()
            ->cache($this->getSlugCacheKey('url', $url), 'slugs')
            ->where([
                $this->alias().'.url' => $url,
            ])
            ->hydrate($this->defaultHydrate)
            ->first();
    }


    /**
     * Finds route entity by url WITHOUT using caching.
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findByUrl(Query $query, array $options) {
        if(!isset($options['url'])) {
            throw new \InvalidArgumentException('This method requires option "url".');
        }
        return $query->where(['url' => $options['url']]);
    }

    /**
     * Finds route entity by slug WITHOUT using caching.
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findBySlug(Query $query, array $options) {
        if(!isset($options['slug'])) {
            throw new \InvalidArgumentException('This method requires option "slug".');
        }
        return $query->where(['slug' => $options['slug']]);
    }

    /**
     * Finds route entity by pk WITHOUT using caching.
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findById(Query $query, array $options) {
        if(!isset($options['id'])) {
            throw new \InvalidArgumentException('This method requires option "id".');
        }
        return $query->where(['id' => $options['id']]);
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getModelsByUrl($value) {
        $key = $this->getSlugCacheKey('url', $value);
        if($this->_cacheEngine !== false) {
            $models = Cache::read($key, $this->_cacheEngine);
            if ($models !== false) {
                return $models;
            }
        }
        $models = $this->find('byUrl', ['url' => $value])->hydrate(false)->all();
        if($models instanceof ResultSet) {
            $models = $models->toArray(); // istnieje problem z zapisywaniem typu \Cake\ORM\ResultSet do cache'a
        }
        if($this->_cacheEngine !== false) {
            Cache::write($key, $models, $this->_cacheEngine);
        }

        return $models;
    }

    /**
     * @param $prefix
     * @param $value
     * @return string
     */
    protected function getSlugCacheKey($prefix, $value) {
        return $prefix . '_' . md5(serialize($value));
    }

    /**
     * @param $caches
     * @return array|bool
     */
    public function invalidateCache($caches) {
        if(!$this->_cacheEngine) {
            return false;
        }

        $cacheKeysToClear = [];
        foreach($caches as $key => $values) {
            foreach($values as $value) {
                $cacheKeysToClear[] = $this->getSlugCacheKey($key, $value);
            }
        }

        return Cache::deleteMany($cacheKeysToClear, $this->_cacheEngine);
    }

    /**
     * Event po utworzeniu/zmodyfikowaniu slugu.
     * Metoda ta usuwa przestarzałe informacje o slug'u z cache'a
     * @param Event $event
     * @param EntityInterface $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        $cacheKeysToClear = []; // nie używamy Cache::delete(), tylko używamy Cache::deleteMany(), które jest szybsze od pojedyńczego usuwania
        $changedAttributes = $entity->extractOriginalChanged($entity->visibleProperties());
        foreach($this->_finders as $finder) {
            if(!isset($cacheKeysToClear[$finder])) {
                $cacheKeysToClear[$finder] = [];
            }
            if(isset($changedAttributes[$finder])) {
                $cacheKeysToClear[$finder][] = $changedAttributes[$finder];
            }
            $cacheKeysToClear[$finder][] = $entity->$finder;
        }

        if(isset($changedAttributes['slug'])) {
            // zróbmy rekursywny update tych encji slug'ów, które prowadziły do właśnie zmienionego slugu...
            foreach($this->find('byUrl', ['url' => $changedAttributes['slug']])->all() as $model) {
                $model->url = $changedAttributes['slug'];
                if($this->save($model)) {
                    $cacheKeysToClear['url'][] = $changedAttributes['slug'];
                    $cacheKeysToClear['slug'][] = $model->slug;
                }
            }
        }

        $this->invalidateCache($cacheKeysToClear);
    }

    /**
     * Force'ujemy tutaj znak "/" przed każdym slugiem.
     *
     * @param Event $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if(isset($data['slug']) && substr($data['slug'], 0, 1) !== '/') {
            $data['slug'] = '/' . $data['slug'];
        }
    }
}