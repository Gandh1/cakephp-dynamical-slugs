<?php

namespace gandh1pl\Cake\Slugs\Routing\Route;

use App\Model\Table\RoutesTable;
use Cake\ORM\TableRegistry;
use Cake\Routing\Exception\RedirectException;
use Cake\Routing\Route\DashedRoute;

/**
 * Class SlugRoute.
 * @package App\Routing\Route
 */
class SlugRoute extends DashedRoute
{
    /**
     * @var RoutesTable|null
     */
    private $routeTable = null;

    /**
     * SlugRoute constructor.
     * @param string $template
     * @param array $defaults
     * @param array $options
     */
    public function __construct($template, $defaults = [], array $options = [])
    {
        parent::__construct($template, $defaults, $options);
        $this->routeTable = TableRegistry::get('Routes');
    }

    /**
     * @param string $url
     * @param string $method
     * @return bool
     */
    public function parse($url, $method = '')
    {
        $url = rtrim($url, '/');
        // do przeszukiwania potrzebujemy sluga bez rozszerzenia
        list($searchUrl, $ext) = $this->_parseExtension($url);

        $route = $this->routeTable->getRouteBySlug($searchUrl);
        if (empty($route)) {
            return parent::parse($url, $method);
        }

        if (!empty($route->redirect)) {
            $newRoute = $this->routeTable->getRouteByUrl($route->url);
            if (!empty($newRoute)) {
                throw new RedirectException($newRoute['slug'], 301);
            }
        }

        return parent::parse($route['url'], $method);
    }

    /**
     * @param array $url
     * @param array $context
     * @return bool|string
     */
    public function match(array $url, array $context = [])
    {
        $matchedUrl = parent::match($url, $context);
        if (empty($matchedUrl)) {
            return $matchedUrl;
        }

        $params = null;
        if (strpos($matchedUrl, '?') !== false) {
            list ($matchedUrl, $params) = explode('?', $matchedUrl);
        }

        $route = $this->routeTable->getRouteByUrl($matchedUrl);
        if (!empty($route)) {
            $matchedUrl = $route['slug'];
        }

        return $this->getUrlWithParams($matchedUrl, $params);
    }

    /**
     * @param string $url
     * @param string $params
     * @return string
     */
    private function getUrlWithParams($url, $params)
    {
        return $url . (!empty($params)? '?'.$params : '');
    }
}