# CakePHP 3 - Dynamical slugs #

This extension provides helper classes to easily implement dynamical slugs in CakePHP 3 Project.

Dynamical slugs means slugs loaded from database.
 
### Configuration ###

- In your `config/routes.php`, instead of `fallbacks` rules use that rule:

~~~~php 
    $routes->connect('/:controller/:action/*', ['action' => 'index'], ['routeClass' => 'gandh1pl\Cake\Slugs\Routing\Route\SlugRoute']);
~~~~

- In `App\Model\Table` namespace create file `RoutesTable.php` and paste code below:
 
~~~~php
 namespace App\Model\Table;
 
 /**
  * Class RoutesTable
  *
  * @package App\Model\Table
  */
 class RoutesTable extends \gandh1pl\Cake\Slugs\Model\Table\RoutesTable
 {
 
     /**
      * Cache engine name. Set to false if you don't wanna to use query's caching.
      * @var string|false
      */
     protected $_cacheEngine = 'slugs';
 
     /**
      * Name of route's table. Defaults to `routes`, but you can change it.
      * @var string
      */
     protected $tableName = 'routes';
 
     /**
      * Preferred hydrate option when making query to database. Defaults to false.
      * @var bool
      */
     protected $defaultHydrate = false;
 }
~~~~

 - In `App\Model\Entity` namespace create file `Route.php` and paste code below:
 
~~~~php
 namespace App\Model\Entity;
 class Route extends \gandh1pl\Cake\Slugs\Model\Entity { }
~~~~

- In table class on which you want to use slugs (for example ArticleTable.php), in `initialize()` method:

~~~~php
        $this->addBehavior('Slug', [
            'className' => '\gandh1pl\Cake\Slugs\Model\Behavior\SlugBehavior',
            'urlTemplate' => '/article/view/:id',
            'urlParameters' => ['id' => 'entity_id'],
            'attributesToGenerateFrom' => ['category.name', 'title', 'id'],
            // 'minimumAttributesToGenerateFrom' => 1,
            // 'generateSlugOnInsert' => true,
            // 'routeRelationBindingKey' => 'route_id',
        ]);

~~~~